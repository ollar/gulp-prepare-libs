const through2 = require('through2');
const rollup = require('rollup');
var resolve = require('rollup-plugin-node-resolve');
var commonjs = require('rollup-plugin-commonjs');
var replace = require('rollup-plugin-replace');

module.exports = () => through2.obj(async function(file, _, _cb) {
    const nodeModulesIndex = file.path.split('/').indexOf('node_modules');
    const packageName = file.path.split('/').splice(nodeModulesIndex + 1, 1);

    const _file = await rollup.rollup({
        input: file.history[0],
        plugins: [
            resolve(),
            commonjs(),
            replace({
                'process.env.NODE_ENV': JSON.stringify('development')
            })
        ]
    }).then(bundle => bundle.generate({ format: 'esm' }));

    file.contents = new Buffer.from(_file.output[0].code);
    file.stem = packageName;

    _cb(null, file);
});